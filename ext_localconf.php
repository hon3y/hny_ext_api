<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtApi',
            'Request',
            [
                'Request' => 'request'
            ],
            // non-cacheable actions
            [
                'Request' => 'request'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    request {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_api') . 'Resources/Public/Icons/user_plugin_request.svg
                        title = LLL:EXT:hive_ext_api/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_api_domain_model_request
                        description = LLL:EXT:hive_ext_api/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_api_domain_model_request.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextapi_request
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder