
plugin.tx_hiveextapi_request {
    view {
        templateRootPaths.0 = EXT:hive_ext_api/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextapi_request.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_api/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextapi_request.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_api/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextapi_request.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextapi_request.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextapi._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-api table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-api table th {
        font-weight:bold;
    }

    .tx-hive-ext-api table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextapi_request {
        settings {
            api {
                request {
                    pid = {$plugin.tx_hiveextapi_request.settings.api.request.pid}
                    token = {$plugin.tx_hiveextapi_request.settings.api.request.token}
                    typeNum = {$plugin.tx_hiveextapi_request.settings.api.request.typeNum}
                }
            }
        }
    }
}

##
## Lib
## API Request
##
lib.tx_hiveextapi_request = COA
lib.tx_hiveextapi_request {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = HiveExtApi
		pluginName = Request
		vendorName = HIVE
		controller = Request
		action = request
		settings =< plugin.tx_hiveextapi_request.settings
		persistence =< plugin.tx_hiveextapi_request.persistence
		view =< plugin.tx_hiveextapi_request.view
	}
}

##
## PAGE for API
##
[PIDinRootline = {$plugin.tx_hiveextapi_request.settings.api.request.pid}] && [globalVar = GP:token={$plugin.tx_hiveextapi_request.settings.api.request.token}]

api_tx_hiveextapi_request = PAGE
api_tx_hiveextapi_request {

    typeNum = {$plugin.tx_hiveextapi_request.settings.api.request.typeNum}
    10 < lib.tx_hiveextapi_request

    config {
        disableAllHeaderCode = 1
        additionalHeaders = Content-type:application/json; charset=utf-8
        xhtml_cleaning = 0
        admPanel = 0
        debug = 0
        no_cache = 1
    }

}

[global]