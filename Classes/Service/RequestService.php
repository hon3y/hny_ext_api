<?php
namespace HIVE\HiveExtApi\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;
/***
 *
 * This file is part of the "hive_ext_api" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Class RequestService
 * @package HIVE\HiveExtApi\Service
 */
class RequestService implements \TYPO3\CMS\Core\SingletonInterface
{
    public function handleRequest ($sNamespace, $sRequestMethod = "", $aRequestArguments = [], $aGetFindBy = []) {

        /*
         * Response array
         */
        $aResponse = [
            'key' => 'value',
            'status' => [200, ''],
            'response' => []
        ];

        switch ($sRequestMethod) {
            case 'GET':
                /*
                 * Only GET
                 */
                break;
            case 'HEAD':
            case 'POST':
            case 'PUT':
            case 'DELETE':
                $aResponse['status'] = [
                    400,
                    'Bad Request. [' . __LINE__ . '] [' . __CLASS__ . '] [' . __FUNCTION__ . ']'
                ];
                return $aResponse;
        }

        if (count($aRequestArguments) < 1 or count($aRequestArguments) > 1) {
            /*
             * Must be one and only one argument!
             */
            $aResponse['status'] = [
                400,
                'Bad Request. Wrong argument count. [' . __LINE__ . '] [' . __CLASS__ . '] [' . __FUNCTION__ . ']'
            ];
        } else {
            foreach ($aRequestArguments as $k => $v) {
                $aResponse['key'] = $k;

                $sDynamicModelNamespace = $sNamespace . "\\Domain\\Model\\";
                $sDynamicRepositoryNamespace = $sNamespace . "\\Domain\\Repository\\";

                /*
                 * Dynamic Model Name
                 */
                $sDynamicModel = str_replace('_', '', ucwords(str_replace("-api", "", $k), '_'));

                /*
                 * Dynamic Model Repsitory
                 */
                $sDynamicRepository = $sDynamicModel . "Repository";

                /*
                 * Instantiate Repository
                 */
                $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
                $oDynamicRepository = $objectManager->get($sDynamicRepositoryNamespace . $sDynamicRepository);

                /*
                 * requested Model and Repository must be available
                 */
                if (class_exists($sDynamicModelNamespace . $sDynamicModel) && class_exists($sDynamicRepositoryNamespace . $sDynamicRepository)) {
                    if ($aRequestArguments[$k] == '') {
                        $aObjects = null;

                        /*
                         * List
                         */
                        if ($aGetFindBy != NULL and is_array($aGetFindBy) and count($aGetFindBy) == 1) {

                            /*
                             * Set up Model
                             */
                            $sRequestModel = $sDynamicModelNamespace . $sDynamicModel;

                            /*
                             * Filter by
                             */
                            $aGetFindBy_ = array_keys($aGetFindBy);
                            $sFindBy = 'findBy' . str_replace('_', '', ucwords($aGetFindBy_[0], '_'));

                            /*
                             * findBy only if property exists in Model
                             */
                            if (property_exists($sRequestModel, $aGetFindBy_[0])) {
                                $aObjects = $oDynamicRepository->$sFindBy($aGetFindBy[$aGetFindBy_[0]]);
                            } else {
                                $aResponse['status'] = [
                                    400,
                                    'Bad Request. Property ' . $aGetFindBy_[0] . ' not available. [' . __LINE__ . '] [' . __CLASS__ . '] [' . __FUNCTION__ . ']'
                                ];
                            }
                        } else {
                            $aObjects = $oDynamicRepository->findAll();
                        }

                        /*
                         * One ore many results found
                         */
                        if ($aObjects != null && count($aObjects) > 0) {
                            $aResponse['response'] = $aObjects;
                        }
                    } else {

                        /*
                         * Show
                         */
                        $oObject = $oDynamicRepository->findByUid(intval($aRequestArguments[$k]));
                        if ($oObject != NULL) {

                            /*
                             * response should be an array even
                             * if we get only one result here
                             */
                            $aResponse['response'][] = $oObject;
                        }
                    }
                } else {
                    $aResponse['status'] = [
                        400,
                        'Bad Request. Classes not available. [' . __LINE__ . '] [' . __CLASS__ . '] [' . __FUNCTION__ . ']'
                    ];
                }
            }
        }
        return $aResponse;
    }
}